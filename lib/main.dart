import 'package:flutter/material.dart';
import 'package:palma_flutter/tabmod/statefullwidget/Tabscreen.dart';
import 'package:palma_flutter/tutorial/statefulwidget/TutorialScreen.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

Future<void> main() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var login = prefs.getBool('login');
  print(login);
  runApp(MaterialApp( debugShowCheckedModeBanner: false,home: login == true ? TabScreen() : MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 2,
      navigateAfterSeconds: new TutorialScreen(),
      imageBackground: AssetImage('assets/background.png'),
    );
  }
}
