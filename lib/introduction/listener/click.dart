import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/login/state/loginscreen.dart';
import 'package:palma_flutter/authentication/signup/state/Signupscreen.dart';
import 'package:palma_flutter/introduction/state/introductionscreen.dart';

void openSignup(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Signupscreen()),
  );
}
void openLogin(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Loginscreen()),
  );
}
