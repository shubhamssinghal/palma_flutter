import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/introduction/listener/click.dart';
import 'package:palma_flutter/introduction/state/introductionscreen.dart';
import 'package:google_sign_in/google_sign_in.dart';

class Introductionstate extends State<Introductionscreen> {
  GoogleSignIn _googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
       Expanded(child:  new Container(
         decoration: new BoxDecoration(
             image: DecorationImage(
                 image: new AssetImage('assets/pathcurve.png'),
                 fit: BoxFit.fill)),
         child: Align(
             alignment: Alignment.center,
             child: Image.asset(
               'assets/logo.png',
               scale: 5,
             )),
       )),
        SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 20),
                child: new Text(
                  'Welcome to Palma',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'GillSans',
                      fontSize: 20,
                      color: Color(0xff2db9b4)),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 10, left: 40, right: 40),
                  child: new Text(
                    'Signup to get started with app.Login if you already have an account.',
                    style: TextStyle(
                      fontFamily: 'GillSans',
                      fontSize: 15,
                    ),
                    textAlign: TextAlign.justify,
                  )),
              Padding(
                padding: EdgeInsets.only(top: 10, left: 40, right: 40),
                child: new ButtonTheme(
                  height: 40,
                  minWidth: 400,
                  child: RaisedButton(
                      onPressed: () {

                      },
                      color: Colors.white,
                      child: new Row(
                        children: <Widget>[
                          Image.asset(
                            'assets/google.png',
                            height: 20,
                            width: 20,
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 70),
                            child: Text(
                              "Signup with google",
                              style: TextStyle(
                                color: Colors.black,
                                fontFamily: 'GillSans',
                                fontSize: 15,
                              ),
                            ),
                          )
                        ],
                      )),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 10, left: 40, right: 40),
                child: new ButtonTheme(
                  height: 40,
                  minWidth: 400,
                  child: RaisedButton(
                    onPressed: () {
                      openSignup(context);
                    },
                    color: Color(0xff2db9b4),
                    child: Text(
                      "Create Account",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'GillSans',
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 2),
                  child: new FlatButton(
                    onPressed: () {
                      openLogin(context);
                    },
                    child: new Text(
                      'Login to my Account',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'GillSans',
                          fontSize: 15,
                          color: Colors.black),
                      textAlign: TextAlign.left,
                    ),
                  ))
            ],
          ),
        ),

      ],
    ));
  }
}
