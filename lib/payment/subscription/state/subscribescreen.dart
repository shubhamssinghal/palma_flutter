import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/login/statefullwidget/loginupstate.dart';
import 'package:palma_flutter/formulatab/statefullwidget/FormulaFState.dart';
import 'package:palma_flutter/home/statefullwidget/HomeFState.dart';
import 'package:palma_flutter/payment/subscription/statefullwidget/subscribeState.dart';
import 'package:palma_flutter/profile/statefullwidget/ProfileState.dart';

class SubscribeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new SubscribeState();
  }
}
