import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/formulatab/model/FormulaModel.dart';
import 'package:palma_flutter/home/state/homefscreen.dart';
import 'package:palma_flutter/payment/manage/state/managescreen.dart';
import 'package:palma_flutter/payment/subscription/state/subscribescreen.dart';
import 'package:palma_flutter/tabmod/statefullwidget/Tabscreen.dart';
import 'package:shared_preferences/shared_preferences.dart';

subscribed() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool('subscribed', true);
}

List formulaList = new List<FormulaListModel>();
bool checkvisiblity = false;

class CustomPopupMenu {
  CustomPopupMenu({this.title, this.icon});

  String title;
  IconData icon;
}

class ManageState extends State<ManageScreen> {
  void _select(CustomPopupMenu choice) {
    setState(() {});
  }

  @override
  void initState() {
    for (int i = 1; i < 3; i++) {
      formulaList
          .add(new FormulaListModel("\$" + i.toString() + "/Month", false));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Stack(
      children: <Widget>[
        new Positioned.directional(
          textDirection: TextDirection.ltr,
          child: Image(
            image: AssetImage('assets/images/curve_background.png'),
          ),
        ),
        new Stack(
          fit: StackFit.expand,
          children: <Widget>[
            new Padding(
              padding: EdgeInsets.only(top: 40),
              child: new Text(
                "Manage Subscription",
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'GillSans',
                  fontWeight: FontWeight.w600,
                  fontSize: 25,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 90),
          child: new ListPlans(),
        )
      ],
    ));
  }
}

class ListPlans extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new ListView.builder(
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(left: 40, right: 40, top: 20),
            child: Stack(
              children: <Widget>[
                new Positioned.directional(
                  textDirection: TextDirection.ltr,
                  child: Image(
                    image: AssetImage('assets/images/payment_card.png'),
                  ),
                ),
                ListTile(
                  title: Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      new Text(
                        formulaList[index].name,
                        style: TextStyle(
                          color: Color(0xff2db9b4),
                          fontSize: 24,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'GillSans',
                        ),
                      ),
                    ],
                  ),
                  trailing: Padding(
                      padding: EdgeInsets.only(top: 0, right: 0),
                      child: IconButton(
                        icon: checkvisiblity
                            ? Image.asset(
                                "assets/images/check.png",
                                scale: 1,
                                alignment: Alignment.center,
                              )
                            : Image.asset(
                                "assets/images/check.png",
                                scale: 1,
                                alignment: Alignment.center,
                              ),
                      )),
                  onTap: () {
                    _tappedFolder(context);
                  },
                )
              ],
            ),
          );
        },
        itemCount: formulaList.length);
  }
}

void _tappedFolder(BuildContext context) {
  subscribed();
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => TabScreen()),
  );
}
