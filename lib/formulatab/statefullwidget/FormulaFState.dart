import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/formulatab/model/FormulaModel.dart';
import 'package:palma_flutter/formulatab/state/formulafscreen.dart';

List formulaList = new List<FormulaListModel>();
bool checkvisiblity = false;
TextEditingController searchformula = new TextEditingController();

class CustomPopupMenu {
  CustomPopupMenu({this.title, this.checked});

  String title;
  bool checked;
}

class FormulaFState extends State<FormulaFScreen> {
  static List<CustomPopupMenu> choices = <CustomPopupMenu>[
    CustomPopupMenu(title: 'A-Z', checked: false),
    CustomPopupMenu(title: 'Z-A', checked: false),
    CustomPopupMenu(title: 'Recently Added', checked: false),
    CustomPopupMenu(title: 'Nestle', checked: false),
    CustomPopupMenu(title: 'Abott', checked: false),
    CustomPopupMenu(title: 'Kate Farms', checked: false),
  ];

  void _select(CustomPopupMenu choice) {
    setState(() {
      choice.checked = true;
      for (var i = 0; i < choices.length; i++) {
        if (choice.title != choices[i].title) {
          choices[i].checked = false;
        }
      }
    });
  }

  @override
  void initState() {
    for (int i = 1; i < 20; i++) {
      formulaList
          .add(new FormulaListModel("Glucerna " + i.toString() + ".0", false));
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Container(
      margin: EdgeInsets.only(top: 40),
      child: Column(
        children: <Widget>[
          Center(
              child: Row(
            children: <Widget>[
              Expanded(
                  child: Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: EdgeInsets.only(left: 40),
                        child: Text(
                          "List of Formulas",
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'GillSans',
                            fontSize: 23,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ))),
              FittedBox(
                fit: BoxFit.cover,
                child: Padding(
                    padding: EdgeInsets.only(right: 20),
                    child: Theme(
                        data: Theme.of(context).copyWith(
                          cardColor: Color(0xff2db9b4),
                        ),
                        child: PopupMenuButton<CustomPopupMenu>(
                          //                    elevation: 3.2,
                          icon: Icon(
                            Icons.filter_list,
                            size: 38,
                          ),
                          onCanceled: () {
                            print('You have not chossed anything');
                          },
                          onSelected: _select,
                          itemBuilder: (BuildContext context) {
                            return choices.map((CustomPopupMenu choice) {
                              return PopupMenuItem<CustomPopupMenu>(
                                  value: choice,
                                  child: Row(
                                    children: <Widget>[
                                      Expanded(
                                        child: Text(
                                          choice.title,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17),
                                        ),
                                        flex: 1,
                                      ),
                                      Expanded(
                                        child: choice.checked
                                            ? Image.asset(
                                                'assets/images/checkedic.png',
                                                scale: 2,
                                                color: Colors.white,
                                              )
                                            : Image.asset(
                                                'assets/images/uncheck.png',
                                                scale: 100,
                                              ),
                                        flex: 0,
                                      ),
                                    ],
                                  ));
                            }).toList();
                          },
                        ))),
              )
            ],
          )),
          Padding(
            padding: EdgeInsets.only(left: 40, right: 40, top: 20, bottom: 0),
            child: TextField(
              keyboardType: TextInputType.text,
              textInputAction: TextInputAction.next,
              controller: searchformula,
              decoration: InputDecoration(
                  suffixIcon: Image.asset(
                    "assets/images/uncheck.png",
                    scale: 3,
                    alignment: Alignment.centerRight,
                  ),
                  labelText: 'Search Here',
                  labelStyle: TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffabb4bd)),
                  ),
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Color(0xffabb4bd)),
                  ),
                  border: new UnderlineInputBorder(
                      borderSide: new BorderSide(color: Color(0xffabb4bd)))),
              style: TextStyle(color: Color(0xff26282e), fontSize: 22),
            ),
          ),
          Expanded(
              child: Card(
                  elevation: 10,
                  margin:
                      EdgeInsets.only(left: 40, right: 40, top: 20, bottom: 20),
                  child: new ListView.separated(
                      itemBuilder: (context, index) {
                        return ListTile(
                          title: Padding(
                            padding: EdgeInsets.only(left: 30),
                            child: new Text(
                              formulaList[index].name,
                              style: TextStyle(
                                color: Color(0xff2db9b4),
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'GillSans',
                              ),
                            ),
                          ),
                          trailing: Padding(
                              padding: EdgeInsets.only(top: 1),
                              child: IconButton(
                                icon: formulaList[index].checked
                                    ? Image.asset(
                                        "assets/images/fav_icon.png",
                                        scale: 4,
                                        alignment: Alignment.center,
                                      )
                                    : Image.asset(
                                        "assets/images/fav_icon.png",
                                        color: Colors.grey,
                                        scale: 4,
                                        alignment: Alignment.center,
                                      ),
                              )),
                          onTap: () => setCheckfun(index),
                        );
                      },
                      separatorBuilder: (context, index) {
                        return Padding(
                            padding:
                                EdgeInsets.only(left: 10, right: 10, top: 0),
                            child: Image.asset("assets/images/line.png"));
                      },
                      itemCount: formulaList.length)))
        ],
      ),
    ));
  }

  setCheckfun(int field) {
    print("" + field.toString());
    setState(() {
      formulaList[field].checked = true;
    });
  }
}
