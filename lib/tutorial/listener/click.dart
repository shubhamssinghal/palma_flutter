import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/introduction/state/introductionscreen.dart';

void openIntroScreen(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Introductionscreen()),
  );
}
