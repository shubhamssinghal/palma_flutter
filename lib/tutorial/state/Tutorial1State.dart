import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/tutorial/statefulwidget/Tutorial1Screen.dart';

class Tutorial1State extends State<Tutorial1Screen> {
  int number = 0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
       Expanded(child:  new Container(
         decoration: new BoxDecoration(
             image: DecorationImage(
                 image: new AssetImage('assets/pathcurve.png'),
                 fit: BoxFit.fill)),
         child: Align(
             alignment: Alignment.center,
             child: Image.asset(
               'assets/tutone.png',
               scale: 5,
             )),
       )),
      ],
    ));
  }
}
