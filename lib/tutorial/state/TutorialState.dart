import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/tutorial/listener/click.dart';
import 'package:palma_flutter/tutorial/statefulwidget/Tutorial1Screen.dart';
import 'package:palma_flutter/tutorial/statefulwidget/Tutorial2Screen.dart';
import 'package:palma_flutter/tutorial/statefulwidget/TutorialScreen.dart';

class TutorialState extends State<TutorialScreen> {
  int counter = 0;

  _onPageViewChange(int page) {
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          child: PageView(
            controller: PageController(
                initialPage: 0, keepPage: true, viewportFraction: 1),
            onPageChanged: _onPageViewChange,
            children: <Widget>[
              Container(
                child: Tutorial1Screen(),
              ),
              Container(
                child: Tutorial2Screen(),
              ),
            ],
            scrollDirection: Axis.horizontal,
          ),
        ),
       /* Container(
        child: DotsIndicator(dotsCount: 2, position: 1),
        ),*/
        Container(
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 0,bottom: 10),
                child: new Text(
                  'Welcome to Palma',
                  style: TextStyle(
                      fontWeight: FontWeight.w600,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'GillSans',
                      fontSize: 20,
                      color: Color(0xff2db9b4)),
                  textAlign: TextAlign.left,
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 5, left: 20, right: 20,bottom:20),
                  child: new Text(
                    'This product guide is intended for use by healthcare professionals. At the time of publication, every effort was made to confirm that all product information was accurate. It is, however, subject to change. The most current information can always be obtained by referring to the package label. The contents of this product guide are for informational purposes only and are not intended to be a substitute for medical advice or clinical judgment.',
                    style: TextStyle(
                      fontFamily: 'GillSans',
                      fontSize: 15,
                    ),
                    textAlign: TextAlign.justify,
                  )),
              Padding(
                padding: EdgeInsets.only(top: 5,bottom: 10),
                child: new ButtonTheme(
                  minWidth: 300.0,
                  height: 40,
                  child: RaisedButton(
                    onPressed: () {
                      openIntroScreen(context);
                    },
                    color: Color(0xff2db9b4),
                    child: Text(
                      "Get Started",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    )
    );
  }
}

/*@override
Widget build(BuildContext context) {
  return new Scaffold(
      body: Column(
    mainAxisSize: MainAxisSize.max,
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Expanded(
        child: PageView(
          controller: PageController(
              initialPage: 0, keepPage: true, viewportFraction: 1),
          onPageChanged: _onPageViewChange,
          children: <Widget>[
            Container(
              child: Tutorial1Screen(),
            ),
            Container(
              child: Tutorial2Screen(),
            ),
          ],
          scrollDirection: Axis.horizontal,
        ),
      ),
      Container(
        child: DotsIndicator(dotsCount: 2, position: counter),
      )
    ],
  ));
}*/
