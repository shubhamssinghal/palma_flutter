import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/tutorial/statefulwidget/Tutorial2Screen.dart';

class Tutorial2State extends State<Tutorial2Screen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: new Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
       Expanded(child:  new Container(

         decoration: new BoxDecoration(
             image: DecorationImage(
                 image: new AssetImage('assets/pathcurve.png'),
                 fit: BoxFit.fill)),
         child: Align(
             alignment: Alignment.center,
             child: Image.asset(
               'assets/tuttwo.png',
               scale: 5,
             )),
       )
       ),
      ],
    ));
  }
}
