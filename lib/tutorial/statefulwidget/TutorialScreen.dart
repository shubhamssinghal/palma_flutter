import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/tutorial/state/TutorialState.dart';

class TutorialScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new TutorialState();
  }
}
