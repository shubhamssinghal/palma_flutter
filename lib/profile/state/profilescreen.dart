import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/login/statefullwidget/loginupstate.dart';
import 'package:palma_flutter/formulatab/statefullwidget/FormulaFState.dart';
import 'package:palma_flutter/home/statefullwidget/HomeFState.dart';
import 'package:palma_flutter/profile/statefullwidget/ProfileState.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ProfileState();
  }
}
