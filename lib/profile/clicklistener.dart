import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_email_sender/flutter_email_sender.dart';
import 'package:palma_flutter/payment/manage/state/managescreen.dart';
import 'package:palma_flutter/webview/state/webscreen.dart';

openManage(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => ManageScreen()),
  );
}

openWeb(BuildContext context, int i) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => WebScreen(i)),
  );
}

sendEmail() async {
  final Email email = Email(
    body: 'Email body',
    subject: 'Email subject',
    recipients: ['example@example.com'],
    cc: ['cc@example.com'],
    bcc: ['bcc@example.com'],
    attachmentPath: '/path/to/attachment.zip',
  );

  await FlutterEmailSender.send(email);
}
