import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/profile/state/profilescreen.dart';

import '../clicklistener.dart';

class ProfileState extends State<ProfileScreen> {
  bool checkvisiblity = false;
  TextEditingController usernamecontroller = new TextEditingController();

  @override
  void initState() {
    checkvisiblity = false;
    usernamecontroller.text = "";
    print("called");
  }

  changeUsername(bool mode) {
    setState(() {
      checkvisiblity = mode;
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        body: Container(
            child: Container(
      child: Column(
        children: <Widget>[
          Flexible(
            flex: 2,
            child: Container(
              padding: EdgeInsets.only(left: 20, right: 20, bottom: 0, top: 40),
              color: Color(0xff2db9b4),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                      child: Padding(
                    padding: EdgeInsets.only(left: 50),
                    child: FittedBox(
                      fit: BoxFit.none,
                      child: Container(
                          width: 100,
                          height: 100,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                  fit: BoxFit.fill,
                                  image: new NetworkImage(
                                      "https://images.megapixl.com/4707/47075253.jpg")))),
                    ),
                  )),
                  Align(
                      child: Container(
                    width: 60,
                    child: checkvisiblity
                        ? GestureDetector(
                            onTap: () {
                              checkvisiblity = false;
                              changeUsername(checkvisiblity);
                              print(
                                  "onTap called." + checkvisiblity.toString());
                            },
                            child: new Text(
                              "Submit",
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'GillSans',
                                decoration: TextDecoration.underline,
                              ),
                            ))
                        : GestureDetector(
                            onTap: () {
                              checkvisiblity = true;
                              changeUsername(checkvisiblity);
                              print(
                                  "onTap called." + checkvisiblity.toString());
                            },
                            child: new Text(
                              "Edit",
                              textAlign: TextAlign.end,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.w600,
                                fontFamily: 'GillSans',
                                decoration: TextDecoration.underline,
                              ),
                            )),
                    alignment: Alignment.topRight,
                  ))
                ],
              ),
            ),
          ),
          Flexible(
            child: Container(
              color: Color(0xff2db9b4),
              child: checkvisiblity
                  ? Container(
                      height: 50,
                      alignment: Alignment.topCenter,
                      margin: EdgeInsets.only(left: 40, right: 40, top: 0),
                      child: new TextField(
                        controller: usernamecontroller,
                        maxLines: 1,
                        textAlign: TextAlign.center,
                        decoration: InputDecoration(
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.white),
                            ),
                            border: new UnderlineInputBorder(
                                borderSide:
                                    new BorderSide(color: Colors.white))),
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'GillSans',
                            fontSize: 23,
                            fontWeight: FontWeight.w700),
                      ),
                    )
                  : Container(
                      height: 50,
                      alignment: Alignment.bottomCenter,
                      child: Text(
                        usernamecontroller.text.length > 0
                            ? usernamecontroller.text
                            : "Alex Moorey",
                        style: TextStyle(
                            color: Colors.white,
                            fontFamily: 'GillSans',
                            fontSize: 23,
                            fontWeight: FontWeight.w700),
                        textAlign: TextAlign.center,
                      ),
                    ),
              alignment: Alignment.topCenter,
            ),
            flex: 0,
            fit: FlexFit.tight,
          ),
          Flexible(
            child: Container(
              color: Color(0xff2db9b4),
              padding: EdgeInsets.only(left: 0, top: 5, bottom: 20),
              child: new Text(
                "alex@gmail.com",
                style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'GillSans',
                  fontSize: 18,
                ),
                textAlign: TextAlign.center,
              ),
              alignment: Alignment.topCenter,
            ),
            flex: 0,
            fit: FlexFit.tight,
          ),
          Flexible(
              flex: 0,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(
                          left: 20, top: 20, bottom: 20, right: 0),
                      child: Image.asset(
                        'assets/images/terms_and_conditions.png',
                        scale: 5,
                      )),
                  Container(
                    child: FlatButton(
                        onPressed: () => openManage(context),
                        child: new Text(
                          "Manage Membership",
                          style: TextStyle(
                            color: Colors.black,
                            fontFamily: 'GillSans',
                            fontSize: 15,
                          ),
                          textAlign: TextAlign.center,
                        )),
                  ),
                ],
              )),
          Flexible(
            child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Image.asset('assets/images/line.png'),
            ),
            flex: 0,
          ),
          Flexible(
              flex: 0,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(
                          left: 20, top: 20, bottom: 20, right: 0),
                      child: Image.asset(
                        'assets/images/terms_and_conditions.png',
                        scale: 5,
                      )),
                  Container(
                      child: FlatButton(
                    onPressed: () => openWeb(context,0),
                    child: new Text(
                      "Terms and Condition",
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'GillSans',
                        fontSize: 15,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )),
                ],
              )),
          Flexible(
            child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Image.asset('assets/images/line.png'),
            ),
            flex: 0,
          ),
          Flexible(
              flex: 0,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(
                          left: 20, top: 20, bottom: 20, right: 0),
                      child: Image.asset(
                        'assets/images/terms_and_conditions.png',
                        scale: 5,
                      )),
                  Container(
                      child: FlatButton(
                    onPressed: () => openWeb(context,1),
                    child: new Text(
                      "Privacy Policy",
                      style: TextStyle(
                        color: Colors.black,
                        fontFamily: 'GillSans',
                        fontSize: 15,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  )),
                ],
              )),
          Flexible(
            child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Image.asset('assets/images/line.png'),
            ),
            flex: 0,
          ),
          Flexible(
              flex: 0,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(
                          left: 20, top: 20, bottom: 20, right: 0),
                      child: Image.asset(
                        'assets/images/info.png',
                        scale: 5,
                      )),
                  Container(
                    child: FlatButton(
                      onPressed: () => openWeb(context,1),
                      child: new Text(
                        "FAQs",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'GillSans',
                          fontSize: 15,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              )),
          Flexible(
            child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Image.asset('assets/images/line.png'),
            ),
            flex: 0,
          ),
          Flexible(
              flex: 0,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(
                          left: 20, top: 20, bottom: 20, right: 0),
                      child: Image.asset(
                        'assets/images/customer.png',
                        scale: 5,
                      )),
                  Container(
                    child: FlatButton(
                      onPressed: () => sendEmail(),
                      child: new Text(
                        "Contact Support",
                        style: TextStyle(
                          color: Colors.black,
                          fontFamily: 'GillSans',
                          fontSize: 15,
                        ),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ],
              )),
          Flexible(
            child: Container(
              margin: EdgeInsets.only(left: 20, right: 20),
              child: Image.asset('assets/images/line.png'),
            ),
            flex: 0,
          ),
          Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: Container(
              alignment: Alignment.bottomCenter,
              margin: EdgeInsets.only(bottom: 10),
              child: new Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  new Positioned(
                    child: RaisedButton(
                        shape: new RoundedRectangleBorder(
                          side: BorderSide(color: Color(0xff2db9b4)),
                          borderRadius: new BorderRadius.only(
                              topLeft: Radius.circular(8),
                              bottomLeft: Radius.circular(8)),
                        ),
                        onPressed: () {},
                        color: Color(0xff2db9b4),
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 15, bottom: 15, left: 110, right: 110),
                          child: Text(
                            "Logout",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'GillSans',
                              fontSize: 15,
                            ),
                          ),
                        )),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    )));
  }
}
