import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

var tac =
    "https://www.termsfeed.com/terms-conditions/bfedd1f0a94ab0e0e408d2d2367040fd";
var pp =
    "https://www.termsfeed.com/privacy-policy/9e8c8e1544d6b551632e017e4c972571";
var faq = "";

int type;

class WebScreen extends StatelessWidget {
  WebScreen(int i) {
    type = i;
  }

  @override
  Widget build(BuildContext context) {
    return WebviewScaffold(
      url: type == 0 ? tac : type == 1 ? pp : faq,
      hidden: true,
    );
  }
}
