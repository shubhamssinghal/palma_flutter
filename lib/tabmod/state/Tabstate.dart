import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/formulatab/state/formulafscreen.dart';
import 'package:palma_flutter/home/state/homefscreen.dart';
import 'package:palma_flutter/profile/state/profilescreen.dart';
import 'package:palma_flutter/tabmod/statefullwidget/Tabscreen.dart';

class TabState extends State<TabScreen> {
  int _cIndex = 0;
  var content = "Home";

  void _incrementTab(index) {
    setState(() {
      _cIndex = index;
      if (index == 0) {
        /*  Toast.show("0", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);*/
      } else if (index == 1) {
        /*Toast.show("1", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);*/
      } else if (index == 2) {
        /*Toast.show("2", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);*/
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: new IndexedStack(
          index: _cIndex,
          children: <Widget>[
            new HomeFScreen(),
            new FormulaFScreen(),
            new ProfileScreen(),
          ],
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _cIndex,
        // this will be set when a new tab is tapped
        type: BottomNavigationBarType.fixed,
        fixedColor: Colors.blueGrey,
        iconSize: 15,

        items: [
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/images/home.png",
              scale: 3,
              color: Colors.black,
            ),
            title: Padding(
                padding: EdgeInsets.only(top: 3),
                child: new Text("Home",
                    style: TextStyle(
                      color: _cIndex == 0 ? Color(0xff2db9b4) : Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'GillSans',
                    ))),
            activeIcon: Image.asset(
              "assets/images/home.png",
              scale: 3,
            ),
          ),
          BottomNavigationBarItem(
            icon: Image.asset(
              "assets/images/tasks_list.png",
              scale: 3,
              color: Colors.black,
            ),
            activeIcon: Image.asset("assets/images/tasks_list.png",
                scale: 3, color: Color(0xff2db9b4)),
            title: Padding(
                padding: EdgeInsets.only(top: 3),
                child: new Text("Formula Listing",
                    style: TextStyle(
                      color: _cIndex == 1 ? Color(0xff2db9b4) : Colors.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'GillSans',
                    ))),
          ),
          BottomNavigationBarItem(
              icon: Image.asset("assets/images/profile.png",
                  scale: 3, color: Colors.black),
              activeIcon: Image.asset("assets/images/profile.png",
                  scale: 3, color: Color(0xff2db9b4)),
              title: Padding(
                  padding: EdgeInsets.only(top: 3),
                  child: new Text("Profile",
                      style: TextStyle(
                        color: _cIndex == 2 ? Color(0xff2db9b4) : Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'GillSans',
                      ))))
        ],
        onTap: (index) {
          _incrementTab(index);
        },
      ),
    );
  }
}
