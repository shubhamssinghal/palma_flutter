import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class TT extends StatelessWidget {
  String tooltip;
  String text;

  TT(this.text, this.tooltip);

  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: tooltip,
      child: Text(text,style: TextStyle(color: Colors.black),),
    );
  }
}
