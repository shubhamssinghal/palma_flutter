import 'package:shared_preferences/shared_preferences.dart';

class SharedPref{
  static  SharedPreferences prefs;
  static init() async {
     prefs = await SharedPreferences.getInstance();
  }
  static setuserLogin(bool USER_LOGIN)  {
    prefs.setBool('login', USER_LOGIN);
  }
  static  getuserLogin()  {
    return prefs.getBool('login');
  }
}