import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/signup/statefullwidget/Signupstate.dart';
import 'package:palma_flutter/introduction/statefulwidget/Introductionstate.dart';

class Signupscreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new Signupstate();
  }
}
