import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:palma_flutter/authentication/login/state/loginscreen.dart';
import 'package:palma_flutter/authentication/signup/state/Signupscreen.dart';
import 'package:palma_flutter/introduction/listener/click.dart';
import 'package:palma_flutter/utils/AppUtils.dart';
import 'package:palma_flutter/utils/ToolTip.dart';
import 'package:toast/toast.dart';

class Signupstate extends State<Signupscreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController cpasswordController = new TextEditingController();
  bool _saving = false;
  bool passwordVisible = false;

  @override
  void initState() {
    passwordVisible = true;
  }

  void openLoginScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Loginscreen()),
    );
  }

  Widget _buildWidget() {
    return new Form(
      child: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                child: TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle:
                      TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                  style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                )),
            Padding(
                padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                child: TextField(
                  obscureText: true,
                  controller: passwordController,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(Icons.info),
                        color: Color(0xff2db9b4),
                        onPressed: () {
                          Center(child: TT("s", "ads"));
/*                        Tooltip(
                          excludeFromSemantics: true,
                          preferBelow: false,
                            message: "Information",child: Text("'Password must be at least 8 characters long, contain a Number’, at-least one ‘Upper case character’ , at-least one ‘Lower case character’  and at-least one ‘special Character’'",style: TextStyle(color: Color(0xff2db9b4)),),);*/
                        },
                      ),
                      labelText: 'Password',
                      labelStyle:
                      TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                  style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                )),
            Padding(
                padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                child: TextField(
                  obscureText: passwordVisible,
                  controller: cpasswordController,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color: Color(0xff2db9b4),
                        ),
                        onPressed: () {
                          setState(() {
                            passwordVisible = !passwordVisible;
                          });
                        },
                      ),
                      labelText: 'Confirm Password',
                      labelStyle:
                      TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                  style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                )),
            Padding(
              padding: EdgeInsets.only(top: 30, left: 40, right: 40),
              child: new ButtonTheme(
                height: 55,
                minWidth: 400,
                child: RaisedButton(
                  onPressed: () {
                    new CircularProgressIndicator();
                    if (validation()) {
                      hitSignup();
                    }
                  },
                  color: Color(0xff2db9b4),
                  child: Text(
                    "Create Account",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'GillSans',
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(top: 20, left: 40, right: 40),
                child: new FlatButton(
                    onPressed: () {
//                    openLogin(context);
                    },
                    child: RichText(
                        text: TextSpan(
                          // set the default style for the children TextSpans
                            style: Theme.of(context)
                                .textTheme
                                .body1
                                .copyWith(fontSize: 15),
                            children: [
                              TextSpan(
                                text: 'By Signing-Up you will be agreeing to the ',
                              ),
                              TextSpan(
                                  text: 'Terms and Conditions',
                                  style: TextStyle(color: Color(0xff2db9b4))),
                              TextSpan(
                                text: ' of use',
                              ),
                            ])))),
            Padding(
                padding: EdgeInsets.only(top: 99, left: 40, right: 40),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    //your elements here
                    new FlatButton(
                        onPressed: () {
                          openLogin(context);
                        },
                        child: RichText(
                            text: TextSpan(
                              // set the default style for the children TextSpans
                                style: Theme.of(context)
                                    .textTheme
                                    .body1
                                    .copyWith(fontSize: 15),
                                children: [
                                  TextSpan(
                                    text: 'Already Have an Account, Log In Here',
                                  ),
                                ])))
                  ],
                ))
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: Color(0xff2db9b4), //change your color here
          ),
          backgroundColor: Colors.white,
          title: Center(
            child:  Text(
              "Create Account",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                fontFamily: 'GillSans',
              ),
            ),
          )
      ),
      body: ModalProgressHUD(child: _buildWidget(), inAsyncCall: _saving),
    );
  }

  void hitSignup() async {
    setState(() {
      _saving = true;
    });

    final userPool = new CognitoUserPool(
        'us-east-2_zhdiU6y2i', '36j3tf0cfobdv4d77cr892rhd0');
    var data;
    try {
      data = await userPool.signUp(
          '' + emailController.text.trim(), '' + passwordController.text.trim());
    } catch (e) {
      Toast.show(e.toString(), context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      setState(() {
        _saving = false;
      });
      print(e);
    }
    print('Code sent to $data');

//    developer.log('Signup Result - ' + data, name: '');
    new Future.delayed(new Duration(seconds: 3), () {
      setState(() {
        //move to nxt screen
        if (data != null) {
          Toast.show("Confirmation link sent to your email id", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          openLoginScreen(context);
          _saving = false;
        } else {
          _saving = false;
        }
      });
    });
  }

  bool validation() {
    if (emailController.text.length == 0) {
      Toast.show("Please fill email", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      return false;
    } else {
      if (!AppUtils.verifyEmail(emailController.text)) {
        Toast.show("Please enter correct email", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        return false;
      } else {
        if (passwordController.text.length == 0) {
          Toast.show("Please fill password", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          return false;
        } else {
          if (!AppUtils.validatePassword(passwordController.text)) {
            Toast.show("Please enter correct password", context,
                duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            return false;
          } else {
            if (passwordController.text != cpasswordController.text) {
              Toast.show("New Password and Confirm Password Mismatch", context,
                  duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
              return false;
            }
          }
          return true;
        }
      }
    }
    return false;
  }
}
