import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/forgetpassword/otp/statefullwidegt/otpscreen.dart';
import 'package:palma_flutter/tabmod/statefullwidget/Tabscreen.dart';

void openOTPScreen(BuildContext context,String emailId) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => OTPscreen(emailId)),
  );
}
void openHomeScreen(BuildContext context) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => TabScreen()),
  );
}
