import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/forgetpassword/emailscreen/state/emailscreenstate.dart';

class Emailscreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new EmailScreenState();
  }
}
