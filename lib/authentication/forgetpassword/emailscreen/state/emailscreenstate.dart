import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:palma_flutter/authentication/forgetpassword/emailscreen/listener/click.dart';
import 'package:palma_flutter/authentication/forgetpassword/emailscreen/statefullwidegt/emailscreen.dart';
import 'package:palma_flutter/utils/AppUtils.dart';
import 'package:toast/toast.dart';

class EmailScreenState extends State<Emailscreen> {
  TextEditingController emailController = new TextEditingController();
  bool _saving = false;

  Widget _buildWidget() {
    return new Form(
        child:SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child:  new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(top: 30, right: 20, left: 20),
                  child: Align(
                      alignment: Alignment.topRight,
                      child: new FlatButton(
                          onPressed: () {
//                    openLogin(context);
                          },
                          child: RichText(
                              text: TextSpan(
                                // set the default style for the children TextSpans
                                  style: Theme.of(context).textTheme.body1.copyWith(
                                      fontSize: 15,
                                      fontWeight: FontWeight.normal,
                                      fontFamily: 'GillSans'),
                                  children: [
                                    TextSpan(
                                        text:
                                        'We can help you reset your password, Please enter the registered email address.',
                                        style: TextStyle(color: Color(0xffabb4bd))),
                                  ]))))),
              Padding(
                  padding: EdgeInsets.only(left: 45, right: 40, top: 30),
                  child: TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                        labelText: 'Email Address',
                        labelStyle: TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                        enabledBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffabb4bd)),
                        ),
                        focusedBorder: UnderlineInputBorder(
                          borderSide: BorderSide(color: Color(0xffabb4bd)),
                        ),
                        border: new UnderlineInputBorder(
                            borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                    style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                  )),
              Padding(
                padding: EdgeInsets.only(top: 30, left: 40, right: 40),
                child: new ButtonTheme(
                  height: 55,
                  minWidth: 400,
                  child: RaisedButton(
                    onPressed: () {
                      new CircularProgressIndicator();
                      if (validation()) {
                        hitSendOTP();
                      }
                    },
                    color: Color(0xff2db9b4),
                    child: Text(
                      "SUBMIT",
                      style: TextStyle(
                        color: Colors.white,
                        fontFamily: 'GillSans',
                        fontSize: 15,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: Color(0xff2db9b4), //change your color here
          ),
          backgroundColor: Colors.white,
          title: Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              "Trouble Signing In?",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                fontFamily: 'GillSans',
              ),
            ),
          )),
      body: ModalProgressHUD(child: _buildWidget(), inAsyncCall: _saving),
    );
  }

  void hitSendOTP() async {
    setState(() {
      _saving = true;
    });

    final userPool = new CognitoUserPool(
        'us-east-2_zhdiU6y2i', '36j3tf0cfobdv4d77cr892rhd0');
    final cognitoUser = new CognitoUser(emailController.text, userPool);
    var data;
    try {
      data = await cognitoUser.forgotPassword();
    } catch (e) {
      Toast.show(e.toString(), context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      setState(() {
        _saving = false;
      });
      print(e);
    }
    print('Code sent to $data');

    new Future.delayed(new Duration(seconds: 3), () {
      setState(() {
        //move to nxt screen
        if (data != null) {
          Toast.show("OTP sent to your email id ", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          openOTPScreen(context, emailController.text.trim());
          _saving = false;
        } else {
          _saving = false;
        }
      });
    });
  }

  bool validation() {
    if (emailController.text.length == 0) {
      Toast.show("Please fill email", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      return false;
    } else {
      if (!AppUtils.verifyEmail(emailController.text)) {
        Toast.show("Please enter correct email", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        return false;
      } else {
        return true;
      }
    }
  }
}
