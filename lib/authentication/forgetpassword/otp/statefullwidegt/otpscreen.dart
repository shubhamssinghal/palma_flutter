import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/forgetpassword/emailscreen/state/emailscreenstate.dart';
import 'package:palma_flutter/authentication/forgetpassword/otp/state/otpscreenstate.dart';

class OTPscreen extends StatefulWidget {
  String email;
  OTPscreen(String emailId)
  {
    email=emailId;
  }


  @override
  State<StatefulWidget> createState() {
    return new OTPScreenState(email);
  }
}
