import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:palma_flutter/authentication/forgetpassword/emailscreen/listener/click.dart';
import 'package:palma_flutter/authentication/forgetpassword/otp/statefullwidegt/otpscreen.dart';
import 'package:palma_flutter/utils/AppUtils.dart';
import 'package:palma_flutter/utils/ToolTip.dart';
import 'package:pin_view/pin_view.dart';
import 'package:toast/toast.dart';

class OTPScreenState extends State<OTPscreen> {
  TextEditingController otpcontroller = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  TextEditingController cpasswordController = new TextEditingController();
  bool _saving = false;
  bool passwordVisible = false;
  String otppin;
  String emailIds;

  OTPScreenState(String emailId) {
    emailIds = emailId;
  }

  @override
  void initState() {
    passwordVisible = true;
  }

  Widget _buildWidget() {
    return new Form(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(top: 30, right: 20, left: 20),
                    child: Align(
                        alignment: Alignment.topRight,
                        child: new FlatButton(
                            onPressed: () {
//                    openLogin(context);
                            },
                            child: RichText(
                                text: TextSpan(
                                  // set the default style for the children TextSpans
                                    style: Theme.of(context).textTheme.body1.copyWith(
                                        fontSize: 15,
                                        fontWeight: FontWeight.normal,
                                        fontFamily: 'GillSans'),
                                    children: [
                                      TextSpan(
                                          text:
                                          'We can help you reset your password, Please enter the 4 digit code which sent on your email address.',
                                          style: TextStyle(color: Color(0xffabb4bd))),
                                    ]))))),
                Padding(
                    padding: EdgeInsets.only(left: 40, right: 40, top: 10),
                    child: PinView(
                        count: 6,
                        autoFocusFirstField: false,
                        margin: EdgeInsets.all(2.5),
                        obscureText: false,
                        style: TextStyle(fontSize: 19.0, fontWeight: FontWeight.w500),
                        dashStyle: TextStyle(fontSize: 25.0, color: Colors.grey),
                        submit: (String pin) {
                          print(pin);
                          otppin = pin;
                        })),
                Padding(
                    padding: EdgeInsets.only(left: 40, right: 40, top: 30),
                    child: TextField(
                      controller: passwordController,
                      obscureText: true,
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                                Icons.info),
                            color:  Color(0xff2db9b4),
                            onPressed: () {
                              Center(child: TT("s","ads"));
/*                        Tooltip(
                          excludeFromSemantics: true,
                          preferBelow: false,
                            message: "Information",child: Text("'Password must be at least 8 characters long, contain a Number’, at-least one ‘Upper case character’ , at-least one ‘Lower case character’  and at-least one ‘special Character’'",style: TextStyle(color: Color(0xff2db9b4)),),);*/
                            },
                          ),
                          labelText: 'New Password',
                          labelStyle: TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffabb4bd)),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffabb4bd)),
                          ),
                          border: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                      style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                    )),
                Padding(
                    padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                    child: TextField(
                      controller: cpasswordController,
                      obscureText: passwordVisible,

                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisible
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color:  Color(0xff2db9b4),
                            ),
                            onPressed: () {
                              setState(() {
                                passwordVisible = !passwordVisible;
                              });
                            },
                          ),
                          labelText: 'Confirm New Password',
                          labelStyle: TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffabb4bd)),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Color(0xffabb4bd)),
                          ),
                          border: new UnderlineInputBorder(
                              borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                      style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                    )),
                Padding(
                  padding: EdgeInsets.only(top: 30, left: 40, right: 40),
                  child: new ButtonTheme(
                    height: 55,
                    minWidth: 400,
                    child: RaisedButton(
                      onPressed: () {
                        if (validation()) {
                          hitchangePassword();
                        }
                      },
                      color: Color(0xff2db9b4),
                      child: Text(
                        "SUBMIT",
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: 'GillSans',
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Padding(
                      padding: EdgeInsets.only(top: 20, left: 40, right: 40),
                      child: new FlatButton(
                          onPressed: () {
//                    openLogin(context);
                          },
                          child: RichText(
                              text: TextSpan(
                                // set the default style for the children TextSpans
                                  style: Theme.of(context)
                                      .textTheme
                                      .body1
                                      .copyWith(fontSize: 15),
                                  children: [
                                    TextSpan(
                                        text: 'Didn’t recieve a verification code? ',
                                        style: TextStyle(
                                            fontWeight: FontWeight.w600,
                                            fontFamily: 'GillSans')),
                                    TextSpan(
                                        text: 'Resend Again',
                                        style: TextStyle(
                                          color: Color(0xff2db9b4),
                                          fontWeight: FontWeight.w600,
                                          fontFamily: 'GillSans',
                                          fontSize: 15,
                                        )),
                                  ])))),
                ),
              ],
            )
        ),);
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: Color(0xff2db9b4), //change your color here
          ),
          backgroundColor: Colors.white,
          title: Padding(
            padding: EdgeInsets.only(left: 20),
            child: Text(
              "Trouble Signing In?",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                fontFamily: 'GillSans',
              ),
            ),
          )),
      body: ModalProgressHUD(child: _buildWidget(), inAsyncCall: _saving),
    );
  }

  bool validation() {
    if (passwordController.text.length == 0) {
      Toast.show("Please fill password", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      return false;
    } else {
      if (!AppUtils.validatePassword(passwordController.text)) {
        Toast.show("Please enter correct password", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        return false;
      } else {
        if (passwordController.text != cpasswordController.text) {
          Toast.show("New Password and Confirm Password Mismatch", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          return false;
        }
      }
      return true;
    }
  }

  void hitchangePassword() async {
    setState(() {
      _saving = true;
    });

    final userPool = new CognitoUserPool(
        'us-east-2_zhdiU6y2i', '36j3tf0cfobdv4d77cr892rhd0');
    final cognitoUser = new CognitoUser(emailIds, userPool);
    bool data;
    try {
      data =
          await cognitoUser.confirmPassword(otppin, cpasswordController.text);
    } catch (e) {
      Toast.show(e.toString(), context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      setState(() {
        _saving = false;
      });
      print(e);
    }
    print('Code sent to $data');

    new Future.delayed(new Duration(seconds: 3), () {
      setState(() {
        //move to nxt screen
        if (data) {
          _saving = false;
          Toast.show("Password Changed Successfully", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          openHomeScreen(context);
        } else {
          _saving = false;
        }
      });
    });
  }

}
