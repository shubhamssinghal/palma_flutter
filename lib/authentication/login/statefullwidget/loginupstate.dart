import 'dart:developer' as developer;

import 'package:amazon_cognito_identity_dart/cognito.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:palma_flutter/authentication/forgetpassword/emailscreen/statefullwidegt/emailscreen.dart';
import 'package:palma_flutter/authentication/login/state/loginscreen.dart';
import 'package:palma_flutter/payment/subscription/state/subscribescreen.dart';
import 'package:palma_flutter/tabmod/statefullwidget/Tabscreen.dart';
import 'package:palma_flutter/utils/AppUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

class Loginstate extends State<Loginscreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();
  bool _saving = false;
  bool passwordVisible = false;

  void openHomeScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TabScreen()),
    );
  }
  void openSubscribe(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SubscribeScreen()),
    );
  }

  void openForgotPassword(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Emailscreen()),
    );
  }

  @override
  void initState() {
    passwordVisible = true;
  }

  Widget _buildWidget() {
    return new Form(
      child:SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child:  new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 40, right: 40, top: 30),
                child: TextField(
                  textInputAction: TextInputAction.next,
                  keyboardType: TextInputType.text,
                  controller: emailController,
                  decoration: InputDecoration(
                      labelText: 'Email',
                      labelStyle:
                      TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                  style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                )),
            Padding(
                padding: EdgeInsets.only(left: 40, right: 40, top: 20),
                child: TextField(
                  obscureText: passwordVisible,
                  keyboardType: TextInputType.text,
                  textInputAction: TextInputAction.next,
                  controller: passwordController,
                  decoration: InputDecoration(
                      suffixIcon: IconButton(
                        icon: Icon(
                          // Based on passwordVisible state choose the icon
                          passwordVisible
                              ? Icons.visibility
                              : Icons.visibility_off,
                          color:  Color(0xff2db9b4),
                        ),
                        onPressed: () {
                          setState(() {
                            passwordVisible = !passwordVisible;
                          });
                        },
                      ),
                      labelText: 'Password',
                      labelStyle:
                      TextStyle(color: Color(0xffabb4bd), fontSize: 17),
                      enabledBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      focusedBorder: UnderlineInputBorder(
                        borderSide: BorderSide(color: Color(0xffabb4bd)),
                      ),
                      border: new UnderlineInputBorder(
                          borderSide: new BorderSide(color: Color(0xffabb4bd)))),
                  style: TextStyle(color: Color(0xff26282e), fontSize: 22),
                )),
            Padding(
                padding: EdgeInsets.only(top: 10, right: 20),
                child: Align(
                    alignment: Alignment.topRight,
                    child: new FlatButton(
                        onPressed: () {
                          openForgotPassword(context);
                        },
                        child: RichText(
                            text: TextSpan(
                              // set the default style for the children TextSpans
                                style: Theme.of(context).textTheme.body1.copyWith(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: 'GillSans'),
                                children: [
                                  TextSpan(
                                      text: 'Forgot Password?',
                                      style: TextStyle(color: Color(0xff2db9b4))),
                                ]))))),
            Padding(
              padding: EdgeInsets.only(top: 10, left: 40, right: 40),
              child: new ButtonTheme(
                height: 55,
                minWidth: 400,
                child: RaisedButton(
                  onPressed: () {
                    new CircularProgressIndicator();
                    if (validation()) {
                      hitLoginApi();
                    }
                  },
                  color: Color(0xff2db9b4),
                  child: Text(
                    "LOG IN",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: 'GillSans',
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 25, left: 40, right: 40,bottom:40),
              child: new ButtonTheme(
                height: 55,
                minWidth: 400,
                child: RaisedButton(
                    onPressed: () {},
                    color: Colors.white,
                    child: new Row(
                      children: <Widget>[
                        Image.asset(
                          'assets/google.png',
                          height: 20,
                          width: 20,
                        ),
                        Padding(
                          padding: EdgeInsets.only(left: 30),
                          child: Text(
                            "LOG IN WITH GOOGLE",
                            style: TextStyle(
                              color: Colors.black,
                              fontFamily: 'GillSans',
                              fontSize: 15,
                            ),
                          ),
                        )
                      ],
                    )),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
          iconTheme: IconThemeData(
            color: Color(0xff2db9b4), //change your color here
          ),
          backgroundColor: Colors.white,
          title: Center(
            child: Text(
              "Log In       ",
              style: TextStyle(
                color: Colors.black,
                fontSize: 25,
                fontWeight: FontWeight.bold,
                fontFamily: 'GillSans',
              ),
            ),
          )),
      body: ModalProgressHUD(child: _buildWidget(), inAsyncCall: _saving),
    );
  }

  bool validation() {
    if (emailController.text.length == 0) {
      Toast.show("Please fill email", context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      return false;
    } else {
      if (!AppUtils.verifyEmail(emailController.text)) {
        Toast.show("Please enter correct email", context,
            duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
        return false;
      } else {
        if (passwordController.text.length == 0) {
          Toast.show("Please fill password", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          return false;
        } else {
          if (!AppUtils.validatePassword(passwordController.text)) {
            Toast.show("Please enter correct password", context,
                duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
            return false;
          } else {}
          return true;
        }
      }
    }
    return false;
  }

  hitLoginApi() async {
    setState(() {
      _saving = true;
    });

    final userPool = new CognitoUserPool(
        'us-east-2_zhdiU6y2i', '36j3tf0cfobdv4d77cr892rhd0');
    final cognitoUser = new CognitoUser('' + emailController.text, userPool);
    final authDetails = new AuthenticationDetails(
        username: '' + emailController.text.trim(),
        password: '' + passwordController.text.trim());

    CognitoUserSession session;
    try {
      session = await cognitoUser.authenticateUser(authDetails);
    } catch (e) {
      Toast.show(e.toString(), context,
          duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
      setState(() {
        _saving = false;
      });
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setBool('login', false);
    }
    developer.log('Login Result - ' + session.isValid().toString(), name: '');
    developer.log('Login Token - ' + session.accessToken.toString(), name: '');
    new Future.delayed(new Duration(seconds: 3), () {
      setState(() async {
        if (session.isValid()) {
          SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setBool('login', true);
          //move to nxt screen
          Toast.show("Success", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);
          openSubscribe(context);
          _saving = false;
        } else {
          //show error
          Toast.show("Fail", context,
              duration: Toast.LENGTH_SHORT, gravity: Toast.BOTTOM);

          setState(() {
            _saving = false;
          });
        }
      });
    });
  }
}
