import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:palma_flutter/authentication/forgetpassword/emailscreen/statefullwidegt/emailscreen.dart';
import 'package:palma_flutter/formulatab/model/FormulaModel.dart';
import 'package:palma_flutter/home/state/homefscreen.dart';
class Company {
  int id;
  String name;

  Company(this.id, this.name);

  static List<Company> getCompanies() {
    return <Company>[
      Company(1, 'Select Goal'),
      Company(2, 'Calories'),
      Company(3, 'Protien'),
      Company(4, 'Water'),
      Company(5, 'Carbohydrate'),
    ];
  }
}
class HomeFState extends State<HomeFScreen> {
  var selected_btn = 0;
  bool visibilityRate = false;
  bool visibilityGoal = false;
  bool checkvisiblity = false;
  List formulaList = new List<FormulaListModel>();
  TextEditingController selectFormula=new TextEditingController();

  List<Company> _companies = Company.getCompanies();
  List<DropdownMenuItem<Company>> _dropdownMenuItems;
  Company _selectedCompany;

  List<DropdownMenuItem<Company>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<Company>> items = List();
    for (Company company in companies) {
      items.add(
        DropdownMenuItem(
          value: company,
          child: Text(company.name),
        ),
      );
    }
    return items;
  }
  onChangeDropdownItem(Company selectedCompany) {
    setState(() {
      _selectedCompany = selectedCompany;
    });
  }
  @override
  void initState() {
    super.initState();
    selected_btn = 0;
    visibilityRate = true;

    for (int i = 1; i < 20; i++) {
      formulaList.add(new FormulaListModel("jevit " + i.toString(), false));
    }

    _dropdownMenuItems = buildDropdownMenuItems(_companies);
    _selectedCompany = _dropdownMenuItems[0].value;
  }

  setCheckfun(int field) {
    print("" + field.toString());
    setState(() {
      formulaList[field].checked = true;
      selectFormula.text=formulaList[field].name;
      for (var i = 0; i < formulaList.length; i++) {
        if (i != field) {
          formulaList[i].checked = false;
        }
      }
    });
  }

  void switchView(bool visibility, String field) {
    setState(() {
      if (field == "rate") {
        visibilityRate = visibility;
      }
      if (field == "goal") {
        visibilityGoal = visibility;
      }
      if (field == "check") {
        checkvisiblity = visibility;
      }
    });
  }

  void openForgotPassword(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => Emailscreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    final _size = MediaQuery.of(context).size;
    print("size" + _size.height.toString());
    return new Scaffold(
        body: Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              Flexible(
                child: Container(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Padding(padding: EdgeInsets.only(top: 83)),
                          RaisedButton(
                              shape: new RoundedRectangleBorder(
                                side: BorderSide(color: Color(0xff2db9b4)),
                                borderRadius: new BorderRadius.only(
                                    topLeft: Radius.circular(8),
                                    bottomLeft: Radius.circular(8)),
                              ),
                              onPressed: () {
                                setState(() {
                                  selected_btn = 0;
//                            visibilityRate ? null : switchView(true, "rate");
                                  visibilityRate = true;
                                  visibilityGoal = false;
                                  switchView(true, "rate");
                                });
                              },
                              color: selected_btn == 0
                                  ? Color(0xff2db9b4)
                                  : Colors.white,
                              child: Padding(
                                padding: EdgeInsets.only(
                                    top: 15, bottom: 15, left: 40, right: 40),
                                child: Text(
                                  "Rate",
                                  style: TextStyle(
                                    color: selected_btn == 0
                                        ? Colors.white
                                        : Color(0xff2db9b4),
                                    fontFamily: 'GillSans',
                                    fontSize: 15,
                                  ),
                                ),
                              )),
                          RaisedButton(
                            shape: new RoundedRectangleBorder(
                              side: BorderSide(color: Color(0xff2db9b4)),
                              borderRadius: new BorderRadius.only(
                                  topRight: Radius.circular(8),
                                  bottomRight: Radius.circular(8)),
                            ),
                            onPressed: () {
                              setState(() {
                                selected_btn = 1;
                                visibilityGoal = true;
                                visibilityRate = false;
                                switchView(true, "goal");
                              });
                            },
                            color: selected_btn == 1
                                ? Color(0xff2db9b4)
                                : Colors.white,
                            child: Padding(
                              padding: EdgeInsets.only(
                                  top: 15, bottom: 15, left: 40, right: 40),
                              child: Text(
                                "Goal",
                                style: TextStyle(
                                  color: selected_btn == 1
                                      ? Colors.white
                                      : Color(0xff2db9b4),
                                  fontFamily: 'GillSans',
                                  fontSize: 15,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                fit: FlexFit.loose,
                flex: 0,
              ),
              Expanded(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Padding(
                        padding: EdgeInsets.only(left: 40, right: 40, top: 0),
                        child: TextField(
                          keyboardType: TextInputType.text,
                          textInputAction: TextInputAction.next,
                          controller: selectFormula,
                          decoration: InputDecoration(
                              suffixIcon: Image.asset(
                                "assets/images/downarrow.png",
                                scale: 3,
                                alignment: Alignment.centerRight,
                              ),
                              labelText: 'Select Formula',
                              labelStyle: TextStyle(
                                  color: Color(0xffabb4bd), fontSize: 17),
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xffabb4bd)),
                              ),
                              focusedBorder: UnderlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xffabb4bd)),
                              ),
                              border: new UnderlineInputBorder(
                                  borderSide: new BorderSide(
                                      color: Color(0xffabb4bd)))),
                          style:
                              TextStyle(color: Color(0xff26282e), fontSize: 22),
                        ))
                  ],
                ),
                flex: 0,
              ),
              Center(
                  child: Padding(
                      padding: EdgeInsets.only(top: 20, left: 40, right: 40),
                      child: Center(
                        child: Card(
                          elevation: 5,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: <Widget>[
                              Center(
                                  child: Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Text(
                                  "Favorties",
                                  style: TextStyle(
                                      color: Color(0xff2db9b4),
                                      fontFamily: 'GillSans',
                                      fontSize: 22,
                                      fontWeight: FontWeight.w600),
                                ),
                              )),
                              Padding(
                                padding: EdgeInsets.only(top: 10),
                                child: Image.asset(
                                  "assets/images/line.png",
                                  alignment: Alignment.center,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Container(
                                height: visibilityRate
                                    ? _size.height * 0.35
                                    : _size.height * 0.25,
                                child: ListView.separated(
                                  shrinkWrap: true,
                                  itemCount: formulaList.length,
                                  itemBuilder: (context, index) {
                                    return ListTile(
                                      title: Text(formulaList[index].name),
                                      trailing: Padding(
                                          padding: EdgeInsets.only(top: 1),
                                          child: IconButton(
                                            icon: formulaList[index].checked
                                                ? Image.asset(
                                                    "assets/images/check.png",
                                                    scale: 4,
                                                    alignment: Alignment.center,
                                                  )
                                                : Image.asset(
                                                    "assets/images/uncheck.png",
                                                    scale: 4,
                                                    alignment: Alignment.center,
                                                  ),
                                            /* onPressed: setCheckfun(
                                                  index)*/
                                          )),
                                      onTap: () => setCheckfun(index),
                                    );
                                  },
                                  separatorBuilder: (context, index) {
                                    return Padding(
                                        padding: EdgeInsets.only(
                                            left: 10, right: 10, top: 0),
                                        child: Image.asset(
                                            "assets/images/line.png"));
                                  },
                                ),
                              ),
                            ],
                          ),
                        ),
                      ))),
              Flexible(
                child: visibilityRate
                    ? new Container(
                        margin:
                            EdgeInsets.only(left: 20, right: 20, bottom: 20),
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Flexible(
                                child: Container(
                              margin: EdgeInsets.only(left: 20, right: 20),
                              child: TextField(
                                keyboardType: TextInputType.number,
                                maxLength: 3,
                                inputFormatters: [
                                  BlacklistingTextInputFormatter(RegExp(
                                      "[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<>?_!@#%^&*()[]"))
                                ],
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                    suffixIcon: Image.asset(
                                      "assets/images/downarrow.png",
                                      scale: 5,
                                      alignment: Alignment.centerRight,
                                    ),
                                    labelText: 'Rate',
                                    labelStyle: TextStyle(
                                        color: Color(0xffabb4bd), fontSize: 17),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xffabb4bd)),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xffabb4bd)),
                                    ),
                                    border: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Color(0xffabb4bd)))),
                                style: TextStyle(
                                    color: Color(0xff26282e), fontSize: 22),
                              ),
                            )),
                            Flexible(
                                child: Container(
                              margin: EdgeInsets.only(left: 20, right: 20),
                              child: TextField(
                                keyboardType: TextInputType.number,
                                maxLength: 3,
                                inputFormatters: [
                                  BlacklistingTextInputFormatter(RegExp(
                                      "[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<>?_!@#%^&*()[]"))
                                ],
                                textInputAction: TextInputAction.next,
                                decoration: InputDecoration(
                                    suffixIcon: Image.asset(
                                      "assets/images/downarrow.png",
                                      scale: 5,
                                      alignment: Alignment.centerRight,
                                    ),
                                    labelText: 'Hours/Day',
                                    labelStyle: TextStyle(
                                        color: Color(0xffabb4bd), fontSize: 17),
                                    enabledBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xffabb4bd)),
                                    ),
                                    focusedBorder: UnderlineInputBorder(
                                      borderSide:
                                          BorderSide(color: Color(0xffabb4bd)),
                                    ),
                                    border: new UnderlineInputBorder(
                                        borderSide: new BorderSide(
                                            color: Color(0xffabb4bd)))),
                                style: TextStyle(
                                    color: Color(0xff26282e), fontSize: 22),
                              ),
                            )),
                          ],
                        ),
                      )
                    : new Container(
                        margin:
                            EdgeInsets.only(left: 20, right: 20, bottom: 20,top: 10),
                        child: Column(
                          children: <Widget>[
                            Container(
                              width: double.infinity,
                              margin: EdgeInsets.only(right: 20, left: 20),
                              child: DropdownButton(
                                value: _selectedCompany,
                                items: _dropdownMenuItems,
                                onChanged: onChangeDropdownItem,
                                isExpanded: true,
                                style: TextStyle(
                                    color: Color(0xffabb4bd), fontSize: 17),
                              ),

                            ),

                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Flexible(
                                    child: Container(
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  child: TextField(
                                    keyboardType: TextInputType.number,
                                    maxLength: 3,
                                    inputFormatters: [
                                      BlacklistingTextInputFormatter(RegExp(
                                          "[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<>?_!@#%^&*()[]"))
                                    ],
                                    textInputAction: TextInputAction.next,
                                    decoration: InputDecoration(
                                        suffixIcon: Image.asset(
                                          "assets/images/downarrow.png",
                                          scale: 5,
                                          alignment: Alignment.centerRight,
                                        ),
                                        labelText: 'Amount',
                                        labelStyle: TextStyle(
                                            color: Color(0xffabb4bd),
                                            fontSize: 17),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xffabb4bd)),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xffabb4bd)),
                                        ),
                                        border: new UnderlineInputBorder(
                                            borderSide: new BorderSide(
                                                color: Color(0xffabb4bd)))),
                                    style: TextStyle(
                                        color: Color(0xff26282e), fontSize: 22),
                                  ),
                                )),
                                Flexible(
                                    child: Container(
                                  margin: EdgeInsets.only(left: 20, right: 20),
                                  child: TextField(
                                    keyboardType: TextInputType.number,
                                    maxLength: 3,
                                    inputFormatters: [
                                      BlacklistingTextInputFormatter(RegExp(
                                          "[abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ<>?_!@#%^&*()[]"))
                                    ],
                                    textInputAction: TextInputAction.next,
                                    decoration: InputDecoration(
                                        suffixIcon: Image.asset(
                                          "assets/images/downarrow.png",
                                          scale: 5,
                                          alignment: Alignment.centerRight,
                                        ),
                                        labelText: 'Hour/Day',
                                        labelStyle: TextStyle(
                                            color: Color(0xffabb4bd),
                                            fontSize: 17),
                                        enabledBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xffabb4bd)),
                                        ),
                                        focusedBorder: UnderlineInputBorder(
                                          borderSide: BorderSide(
                                              color: Color(0xffabb4bd)),
                                        ),
                                        border: new UnderlineInputBorder(
                                            borderSide: new BorderSide(
                                                color: Color(0xffabb4bd)))),
                                    style: TextStyle(
                                        color: Color(0xff26282e), fontSize: 22),
                                  ),
                                )),
                              ],
                            ),
                          ],
                        )),
                flex: 0,
              ),
              Flexible(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(bottom: 20),
                      child: RaisedButton(
                          shape: new RoundedRectangleBorder(
                            side: BorderSide(color: Color(0xff2db9b4)),
                            borderRadius: new BorderRadius.only(
                                topLeft: Radius.circular(8),
                                bottomLeft: Radius.circular(8)),
                          ),
                          onPressed: () {},
                          color: Color(0xff2db9b4),
                          child: Padding(
                            padding: EdgeInsets.only(
                                top: 15, bottom: 15, left: 110, right: 110),
                            child: Text(
                              "Calculate",
                              style: TextStyle(
                                color: Colors.white,
                                fontFamily: 'GillSans',
                                fontSize: 15,
                              ),
                            ),
                          )),
                    )
                  ],
                ),
                flex: 0,
              ),
            ],
          ),
        )
      ],
    ));
  }
}
