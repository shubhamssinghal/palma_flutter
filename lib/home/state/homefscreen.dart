import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:palma_flutter/authentication/login/statefullwidget/loginupstate.dart';
import 'package:palma_flutter/home/statefullwidget/HomeFState.dart';

class HomeFScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new HomeFState();
  }
}
